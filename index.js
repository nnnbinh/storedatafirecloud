const express = require('express');

const app = express();

const expressLayouts = require('express-ejs-layouts');
const cors = require('cors');
const bodyParser = require('body-parser');
const methodOverride = require('method-override');
const session = require('express-session');
const flash = require('connect-flash');
const passport = require('passport');
const localStrategy = require('passport-local').Strategy;
const path = require('path');
const config = require('./config');
const bcrypt = require('bcrypt')

global.basedir = __dirname;

// routes
const studentRoutes = require('./routes/student-routes');
const versionRoutes = require('./routes/version-routes');
const loginRoutes = require('./routes/login-routes');
const reportRoutes = require('./routes/report-routes');
const fileRoutes = require('./routes/file-routes');
const maintenanceRoutes = require('./routes/maintenance-routes');

// static files
app.use('/assets', express.static(path.join(__dirname, 'assets')));
app.use('/uploads', express.static(path.join(__dirname, 'uploads')));

// middleware
app.set('view engine', 'ejs');
app.use(expressLayouts);
app.use(flash());
app.use(express.json({ limit: '5mb' }));
app.use(cors());
app.use(bodyParser.urlencoded({ extended: true, limit: '5mb' }));
// app.use(upload.array());
app.use(methodOverride('_method'));
app.use(
  session({
    secret: 'something',
    resave: false,
    saveUninitialized: true,
    cookie: {
      maxAge: 60 * 60 * 24 * 1000,
    },
  }),
); app.use(passport.initialize());
app.use(passport.session());

const { db } = require('./db');

const firestore = db.firestore();
// passport
passport.serializeUser((user, done) => {
  done(null, user.id);
});

passport.deserializeUser(async (id, done) => {
  try {
    const user = await firestore.collection('users').doc(id).get();
    done(null, user.data());
  } catch (error) {
    done(error);
  }
});
passport.use(new localStrategy(async (username, password, done) => {
  try {

    const users = await firestore.collection('users').where('username', '==', username).limit(1);
    const data = await users.get();
    if (data.empty) {
      return done(null, false, { message: 'Incorrect username or password' });
    }
    let user = {};
    data.forEach((element) => {
      user = element.data();
    });
    const validPassword = await bcrypt.compare(password,user.password);
    if(validPassword){
      return done(null, user);
    }else{      
      return done(null, false, { message: 'Incorrect username or password' });
    }
  } catch (error) {
    return done(error);
  }
}));
// end passport
// Routers

app.use(loginRoutes.routes);
app.use(studentRoutes.routes);
app.use(versionRoutes.routes);
app.use(reportRoutes.routes);
app.use(fileRoutes.routes);
app.use(maintenanceRoutes.routes);

app.use((error, req, res) => {
  console.log("Error Handling Middleware called")
  console.log('Path: ', req.path)
  console.error('Error: ', error)
  console.log(error.type)

  if (error.type === 'time-out') {// arbitrary condition check
      res.status(408).send("timeout")
  }
  else {
      res.status(500).send(error.type)
  }
})


app.listen(config.port, () => console.log(`App is listening on port: ${config.port}`));
