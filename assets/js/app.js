
function validateForm() {
    var agree=confirm("Bạn có muốn xóa đơn?");
    if (agree)
    return true ;
    else
    return false ;
}

function confirmUpdate(){
    const student_android = document.getElementById("student_version_android").value;
    const student_ios = document.getElementById("student_version_ios").value;
    const lecture_android = document.getElementById("lecture_version_android").value;
    const lecture_ios = document.getElementById("lecture_version_ios").value;
    const message = `Do you want to update: \n student_android: ${student_android} \n student_ios: ${student_ios} \n lecture_android: ${lecture_android}\n lecture_ios: ${lecture_ios}`;
    
    return confirm(message);
}

function resetForm(){
    document.getElementById("versionForm").reset();
}

function myFunction(){
    var input, filter, card, li, a, i, txtValue;
    input = document.getElementById("myInput");
    filter = input.value.toUpperCase();
    card = document.getElementById("my-card");
    col = card.getElementsByClassName("img-col");
    for (i = 0; i < col.length; i++) {
        header = col[i].getElementsByClassName("card-header") 
        txtValue = header[0].textContent || header[0].innerText;
        if (txtValue.toUpperCase().indexOf(filter) > -1) {
            col[i].style.display = "";
        } else {
            col[i].style.display = "none";
        }
    }
}

// window.history.pushState({}, document.title, "/" + "manager");
