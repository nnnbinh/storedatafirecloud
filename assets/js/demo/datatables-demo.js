// Call the dataTables jQuery plugin
$(document).ready(function() {
  $('#dataTable').DataTable({
    "order": [[ 4, "desc" ]],
    "columnDefs": [
      { "width": "50%", "targets": 1 },
    ]
  });
  $('#dataTable-forms').DataTable({
    "order": [[ 3, "desc" ]],
  });
});
