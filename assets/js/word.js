const type = document.getElementById("type").textContent;
var today = new Date();
var year = today.getFullYear();
const f001= ()=> {
  return {
    ten: document.getElementById("Ten").textContent||" ",
    mssv: document.getElementById("mshv").textContent||" ",
    ngaysinh: document.getElementById("NgaySinh").textContent||" ",
    diachi:
      document.getElementById("address").textContent||" " +
      " " +
      document.getElementById("ward").textContent||" " +
      " " +
      document.getElementById("district").textContent||" " +
      " " +
      document.getElementById("city").textContent||" ",
    xa: document.getElementById("ward").textContent||" ",
    huyen: document.getElementById("district").textContent||" ",
    tinh: document.getElementById("city").textContent||" ",
    namthu: year - document.getElementById("NamNhapHoc").textContent||" ",
    lop: document.getElementById("MaLop").textContent||" ",
    nganh: document.getElementById("KhoiNganh").textContent||" ",
    khoa: document.getElementById("KhoaHoc").textContent||" ",
    nambatdau: document.getElementById("NamNhapHoc").textContent||" ",
    namketthuc: "2022",
    kyluat:
      document.getElementById("discipline").textContent == "true"
        ? "Có"
        : "Không",
  };
};
const f002 =()=> {
  return {
    ten: document.getElementById("ten").textContent||" ",
    mssv: document.getElementById("mshv").textContent||" ",
    gioitinh: document.getElementById("gioitinh").textContent||" ",
    ngaysinh: document.getElementById("ngaysinh").textContent||" ",
    noisinh: "TPHCM",
    lop: document.getElementById("malop").textContent||" ",
    khoa: document.getElementById("khoiNganh").textContent||" ",
    nienkhoa: document.getElementById("khoahoc").textContent||" ",
    he: "Cao đẳng chính quy",
    hocki: "2",
    namthu: "3",
    lydo: "thich",
    kyluat:
      document.getElementById("discipline").textContent == "true"
        ? "Có"
        : "Không",
  };
};

function loadFile(url, callback) {
  PizZipUtils.getBinaryContent(url, callback);
}
function generate() {
  loadFile(
    "../../assets/word template/" + type + ".docx",
    function (error, content) {
      if (error) {
        throw error;
      }
      var zip = new PizZip(content);
      var doc = new window.docxtemplater(zip, {
        paragraphLoop: true,
        linebreaks: true,
      });

      // Render the document (Replace {first_name} by John, {last_name} by Doe, ...)
      switch (type) {
        case "f001":
            doc.render(f001());
            break;
        case "f002":
            doc.render(f002());
            break;
        default:
            break;
      }
      var out = doc.getZip().generate({
        type: "blob",
        mimeType:
          "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
        // compression: DEFLATE adds a compression step.
        // For a 50MB output document, expect 500ms additional CPU time
        compression: "DEFLATE",
      });
      // Output the document using Data-URI
      saveAs(out, "output.docx");
    }
  );
}


function canApproved(){
  var staff = document.getElementById("staff").value
  if(staff.trim() == ""){
    alert("Chưa có nhân viên xử lý");
    return false;
  }else{
    document.getElementById("staff_approved").value=staff;
  }  
}

function canRejected(){
  var staff = document.getElementById("staff").value;
  var reason = document.getElementById("reason_reject").value;
  if(staff.trim() == ""){
    alert("Chưa có nhân viên xử lý");
    return false;
  }else if(reason.trim()==""){
    alert("Chưa có lý do");
    return false;
  }else{
    document.getElementById("staff_rejected").value=staff;
  }
  
}