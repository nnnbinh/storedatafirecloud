const express = require("express");
const bcrypt = require("bcrypt")
const router = express.Router();
const passport = require('passport');
const localStrategy = require('passport-local').Strategy;
const { isLoggedIn } = require('../controllers/authController');
const {
  index
} = require("../controllers/indexController");

const {
  isLoggedOut,
  passportAuth,
  logIn,
  logOut,
} = require("../controllers/authController");

router.get("/", isLoggedIn,index);
router.get("/login", isLoggedOut, logIn);
router.post("/login", passportAuth);
router.get("/logout", logOut);

router.get("/signup",async(req,res)=>{
  const salt = await bcrypt.genSalt(10);
  // now we set user password to hashed password
  const validPassword = await bcrypt.hash("admin", salt);
  res.send(validPassword)
})
module.exports = {
  routes: router,
};
