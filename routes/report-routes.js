const express = require("express");
const {
  getAllReports,
  deleteReport
} = require("../controllers/reportController");
const { isLoggedIn } = require('../controllers/authController');
const router = express.Router();

router.get("/reports",isLoggedIn ,getAllReports );
router.delete("/reports/:id",isLoggedIn, deleteReport);
// router.get("/reports/:id", getReport);

module.exports = {
  routes: router,
}; 
