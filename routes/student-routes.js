const express = require("express");
const {
  addStudent,
  getAllStudent,
  getStudent,
  updateStudent,
  deleteStudent,
} = require("../controllers/formControllers");
const { isLoggedIn } = require('../controllers/authController');

const router = express.Router();

router.post("/form" ,addStudent);
router.get("/manager/",isLoggedIn ,getAllStudent);
router.get("/form/:id",isLoggedIn ,getStudent);
router.post("/form/:id",isLoggedIn ,updateStudent);
router.delete("/form/:id",isLoggedIn ,deleteStudent);

module.exports = {
  routes: router,
};
