const express = require("express");
const router = express.Router();
const { isLoggedIn } = require('../controllers/authController');


router.get("/maintenance",isLoggedIn ,(req,res)=>{
    res.render('pages/maintenance',{
        title: "Maintenance Dashboard",
        username: req.user.username,
        message: req.flash("message"),
    })
});

module.exports = {
    routes: router,
  };
