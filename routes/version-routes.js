const express = require("express");
const {
  checkBuildVersion,
  updateVersion,
  studentVersionIOS,
  lectureVersionAndroid,
  lectureVersionIOS,
  studentVersionAndroid
} = require("../controllers/versionController");
const { isLoggedIn } = require('../controllers/authController');
const router = express.Router();

router.get("/checkbuildversion", studentVersionAndroid );
router.get("/version/manager",isLoggedIn ,checkBuildVersion);
router.post("/version/manager",isLoggedIn ,updateVersion,checkBuildVersion);
router.get('/version/android/lecturer',lectureVersionAndroid);
router.get('/version/android/student',studentVersionAndroid );
router.get('/version/ios/lecturer',lectureVersionIOS);
router.get('/version/ios/student',studentVersionIOS)

module.exports = {
  routes: router,
};
