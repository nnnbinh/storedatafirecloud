const express = require("express");
const router = express.Router();
const {
  upload,
  getListFiles,
  // download
  getUserImg
} = require("../controllers/fileController");
const { isLoggedIn } = require('../controllers/authController');


router.post("/upload", upload);
router.get("/gallery",isLoggedIn ,getListFiles);
// router.get("/files/:name", download);
router.get("/user/id/:id",getUserImg);

module.exports = {
    routes: router,
  };
