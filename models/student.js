class Student {
  constructor(id, mshv, ten, gioitinh, nam_sinh,noi_sinh,ho_khau,lop,khoa,nien_khoa,he,hoc_ky,nam,ly_do,ky_luat,status) {
    this.id = id;
    this.mshv=mshv;
    this.ten = ten;
    this.gioitinh = gioitinh;
    this.nam_sinh = nam_sinh;
    this.noi_sinh = noi_sinh;
    this.status = status;
    this.ho_khau= ho_khau;
    this.lop=lop;
    this.khoa=khoa;
    this.nien_khoa=nien_khoa;
    this.he=he;
    this.hoc_ky=hoc_ky;
    this.nam=nam;
    this.ly_do=ly_do;
    this.ky_luat=ky_luat;
  }
}

module.exports = Student;
