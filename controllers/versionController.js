"use strict";
const {admin,db} = require("../db");
const firestore = db.firestore();

const studentVersionIOS = async(req,res)=>{
  try {
    const id = "latest_version";
    const data = await firestore.collection("App Version").doc(id).get();

    res.json({version: data.data().student_version_ios})
  } catch (error) {
    res.send("loi o day");
  }
}

const lectureVersionIOS = async(req,res)=>{
  try {
    const id = "latest_version";
    const data = await firestore.collection("App Version").doc(id).get();

    res.json({version: data.data().lecture_version_ios})
  } catch (error) {
    res.status(400).send(error.message);
  }
}

const lectureVersionAndroid = async(req,res)=>{
  try {
    const id = "latest_version";
    const data = await firestore.collection("App Version").doc(id).get();

    res.json({version: data.data().lecture_version_android})
  } catch (error) {
    res.send(error.message);
  }
}

const studentVersionAndroid = async (req, res) => {
  try {
    const id = "latest_version";
    const data = await firestore.collection("App Version").doc(id).get();

    res.json({version: data.data().student_version_android})
  } catch (error) {
    res.send(error.message);
  }
};

const checkBuildVersion = async (req, res) => {
  try {
    const id = "latest_version";
    const data = await firestore.collection("App Version").doc(id).get();
    const version = {
      student_android: data.data().student_version_android || "",
      student_ios: data.data().student_version_ios || "",
      lecture_android: data.data().lecture_version_android || "",
      lecture_ios: data.data().lecture_version_ios || "",
    };
    res.render("pages/version",{version,title:'Version manager',username:req.user.username,message: req.flash('message')
  })
  } catch (error) {
    res.status(400).send(error.message);
  }
};

const updateVersion = async (req, res,next) => {
  try {
    const id = "latest_version";
    const new_version = req.body;
    const version = await firestore.collection("App Version").doc(id);
    await version.update(new_version);
    req.flash("message","Update successful")
    next()
  } catch (error) {
    res.status(400).send(error.message);
  }
};

module.exports = {
  checkBuildVersion,
  updateVersion,
  studentVersionIOS,
  lectureVersionAndroid,
  lectureVersionIOS,
  studentVersionAndroid
};
