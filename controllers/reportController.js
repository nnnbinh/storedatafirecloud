"use strict";

const {admin,db} = require("../db");
const firestore = db.firestore();
const { HOST_URL } = process.env;

const collection ="Report Error"

const getAllReports = async (req, res) => {
  try {
    const reports = await firestore.collection(collection);
    const data = await reports.get();
    const reportArrays = [];
    if (data.empty) {
      res.render("pages/404",{username:req.user.username,message: req.flash('message'),title: "Reports"})
    } else {
      data.forEach((element) => {
        if(element.id!=="count"){
        const report = {
            id: element.id,
            feature: element.data().feature.charAt(0).toUpperCase() + element.data().feature.slice(1),
            content: element.data().content.text,
            model: element.data().model,
            serialNumber: element.data().serialNumber,
            time: newTime(element.data().time),
          };
          reportArrays.push(report);
        }
      });
      res.render("pages/getAllReport", {
        reports: reportArrays,
        title: "Reports",
        username: req.user.username,
        message: req.flash('message')
      });
    }
  } catch (error) {
    res.status(400).send(error.message);
  }
};

// const getReport = async (req, res) => {
//   try {
//     const id = req.params.id;
//     const report = await firestore.collection(collection).doc(id);
//     const data = await report.get();
//     if (!data.exists) {
//       res.status(400).send("student id: " + id + " don't exist");
//     } else {
//       const detail={
//         id,
//         feature: data.data().feature,
//         content: data.data().content.text,
//         model: data.data().model,
//         serialNumber: data.data().serialNumber,
//         time: data.data().time
//       }
//       res.render("pages/getReport", { report: detail,title:'Report Details',username:"req.user.username"});
//     }
//   } catch (error) {
//     res.status(400).send(error.message);
//   }
// };

const deleteReport = async (req, res) => {
  try {
    const id = req.params.id;
    await firestore.collection(collection).doc(id).delete();
    await firestore.collection(collection).doc('count').update({counters: admin.firestore.FieldValue.increment(-1)})
    req.flash("message","Delete successful");
    res.redirect("/reports");
  } catch (error) {
    res.status(400).send(error.message);
  }
};

function newTime(time){
  const x = time.slice(6) +" " +time.slice(0,5)
  return x
}

module.exports = {
  // getReport,
  getAllReports,
  deleteReport
};
