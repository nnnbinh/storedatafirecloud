"use strict";

const {db} = require("../db");
const firestore = db.firestore();
const { HOST_URL } = process.env;

const getTotalForms = async () => {
    try {
        const total = await firestore.collection("ApplicationForm").doc("count").get()
        return total.data()
      } catch (error) {
        throw error
      }
};
const getTotalReports = async (req, res) => {
  try {
    const total = await firestore.collection("Report Error").doc("count").get()
    return total.data().counters
  } catch (error) {
    throw error
  }
};

const getbuildVersion = async()=>{
  const id = "latest_version";
  const data = await firestore.collection("App Version").doc(id).get();

  return {
    studentAndroid: data.data().student_version_android,
    studentIos: data.data().student_version_ios,
    lectureAndroid: data.data().lecture_version_android,
    lectureIos: data.data().lecture_version_ios,
  }
}

const getPendingForms = async (req, res) => {

};

const index = async(req,res)=>{
  try{
  const totalForms = await getTotalForms();
  const totalReports = await getTotalReports();
  const buildVersion = await getbuildVersion();
  const counters = {
    totalForms,
    totalReports,
    buildVersion
  }
  res.render("pages/index",{counters,title:'index',username:req.user.username,message: req.flash('message')});
}catch(error){
  res.status(400).send(error.message)
}
}

module.exports = {
    index
};
