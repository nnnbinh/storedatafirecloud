// const uploadFile = require("../middleware/upload");
const fs = require("fs");
const mime = require("mime");
const { v4: uuidv4 } = require("uuid");
const { HOST_URL } = process.env;
const {admin,db} = require("../db");
const firestore = db.firestore();

const collection = "User Sync";

const upload = async (req, res) => {
  const directoryPath = basedir + "/uploads/";
  try {
    // await uploadFile(req,res);
    res.setTimeout(300);
    var matches = req.body.base64image.match(
      /^data:([A-Za-z-+/]+);base64,(.+)$/
    );
    const userId = req.body.userId.trim();
    response = {};

    if (matches.length !== 3) {
      return res.status(400).send("Invalid input string");
    }
    response.type = matches[1];
    response.data = new Buffer.from(matches[2], "base64");
    let decodedImg = response;
    let imageBuffer = decodedImg.data;
    let type = decodedImg.type;
    let extension = mime.extension(type);
    let fileName = userId + "-" + uuidv4() + "." + extension;

    const test = await firestore.collection(collection).doc(userId).get();
    if (test.exists) {
      let url = test.data().img;
      const old_name = url.replace(HOST_URL+"/uploads/", "");
      const new_name = "replaced" + "-" + old_name.slice(old_name.indexOf("-") + 1);
      try {
        fs.rename(directoryPath+old_name, directoryPath+new_name, (err) => {
          if (err) throw err;
        });
      } catch (error) {
        res
          .status(500)
          .send({ message: `Could not rename the file: ${error.message}` });
      }
    }

    try {
      fs.writeFileSync("./uploads/" + fileName, imageBuffer, "utf8");
    } catch (e) {
      res.status(500).send({
        message: `Could not upload the file: ${e}`,
      });
    }

    const data = {
      img: HOST_URL + "/uploads/" + fileName,
      userId,
    };

    await firestore.collection(collection).doc(userId).set(data);
    res.status(200).send({
      message: "Upload the file successfully",
    });
  } catch (err) {
    if (err.code == "LIMIT_FILE_SIZE") {
      return res.status(500).send({
        message: "File size cannot be larger than 2MB!",
      });
    }
    res.status(500).send({
      message: `Could not upload the file: ${err}`,
    });
  }
};

const getListFiles = async (req, res) => {
  const directoryPath = basedir + "/uploads";
  const fileInfos = [];
  fs.readdir(directoryPath, function (err, files) {
    if (err) {
      res.status(500).send({
        message: "Unable to scan file",
      });
    }
    files.forEach((file) => {
      const userId = file.slice(0, file.indexOf("-"));
      if (userId !== "replaced") {
        fileInfos.push({
          img: file,
          userId: userId,
        });
      }
    });
    if (fileInfos.length < 1) {
      res.render("pages/404", {
        username: req.user.username,
        message: req.flash("message"),
        title: "Images",
      });
    }
    res.render("pages/getAllImage", {
      imgArrays: fileInfos,
      title: "Gallery",
      username: req.user.username,
      message: req.flash("message"),
    });
  });
};

// const download = (req, res) => {
//   const fileName = req.params.name;
//   const directoryPath = __basedir + "/uploads/";

//   res.download(directoryPath + fileName, fileName, (err) => {
//     if (err) {
//       res.status(500).send({
//         message: "Could not download the file" + err,
//       });
//     }
//   });
// };

const getUserImg = async (req, res) => {
  try {
    const id = req.params.id;
    const student = await firestore.collection(collection).doc(id);
    const data = await student.get();
    if (!data.exists) {
      res.json({ success: false, img: null });
    } else {
      res.json({ success: true, img: data.data().img });
    }
  } catch (error) {
    res.status(400).send(error.message);
  }
};

module.exports = {
  upload,
  getUserImg,
  //   download,
  getListFiles,
};
