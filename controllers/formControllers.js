"use strict";

const {admin,db} = require("../db");
const firestore = db.firestore();
const { HOST_URL } = process.env;
const collection ="ApplicationForm"
const PizZip = require("pizzip");
const Docxtemplater = require("docxtemplater");
var stream = require("stream");

const fs = require("fs");
const path = require("path");

const tenMaudon = [
  { id: "f001", name: "Đơn tạm hoãn nghĩa vụ quân sự" },
  { id: "f002", name: "Giấy chứng nhận sinh viên" },
  { id: "f003", name: "Đơn xin gia hạn học phí" },
  { id: "f004", name: "Đơn xác nhận đóng học phí" },
  { id: "f005", name: "Đơn đăng ký học lại-thi lại" },
  { id: "f006", name: "Đơn xác nhận nợ môn học" },
  { id: "f007", name: "Đơn xin vay vốn ngân hàng chính sách" },
  { id: "f008", name: "Đơn cam kết trả nợ học phí" },
  { id: "f009", name: "Đơn xin cấp bù học phí" },
  { id: "f010", name: "Giấy xác nhận miễn giảm học phí ngành độc hại" },
];

const addStudent = async (req, res) => {
  try {
    const data = req.body;
    await firestore.collection(collection).doc('count').update({counters: admin.firestore.FieldValue.increment(1),pending:admin.firestore.FieldValue.increment(1)})
    await firestore
      .collection(collection)
      .add(data)
      .then((docRef) => {
        const link = {
          success: true,
          url: HOST_URL + "/form/" + docRef.id,
        };
        res.send(link);
      });
  } catch (error) {
    await firestore.collection(collection).doc('count').update({counters: FieldValue.increment(-1)})
    res.status(400).send(error.message);
  }
};

const getAllStudent = async (req, res) => {
  try {
    const search_mssv = req.query.search_mssv || "";
    const search_don = req.query.search_don === undefined || req.query.search_don === "Tất cả" ? "" : req.query.search_don;
    const search_status =
      req.query.search_status === undefined || req.query.search_status === "-1"
        ? ""
        : req.query.search_status;
    const students = await firestore.collection(collection);
    const data = await students.get();

    const studentArrays = [];
    if (data.empty) {
      res.render("pages/404",{username:req.user.username,message: req.flash('message'),title: "Manager"})
    } else {
      data.forEach((element) => {
        if(element.id !=="count"){
          const student = {
            id: element.id,
            status: element.data().status || 0,
            type: element.data().type
              ? tenMaudon.find(({ id }) => id === element.data().type).name
              : "",
            mshv: element.data().mshv || "",
            time: element.data().time ? newTime(element.data().time) : "",
            staff: element.data().staff || "",
          };
          studentArrays.push(student);
        }
      });

      const Filter = studentArrays.filter((student) => {
        return (
          student.mshv.toString().toLowerCase().includes(search_mssv.toLowerCase()) &&
          student.type.toLowerCase().includes(search_don.toLowerCase()) &&
          student.status.toString().includes(search_status)
        );
      });
      res.render("pages/getAllStudent", {
        students: Filter,
        search_mssv: search_mssv,
        search_don: search_don,
        title: "Manager",
        username:req.user.username,
        message: req.flash('message')
      });
    }
  } catch (error) {
    res.status(400).send(error.message);
  }
};

const getStudent = async (req, res) => {
  try {
    const id = req.params.id;
    const student = await firestore.collection(collection).doc(id);
    const data = await student.get();
    if (!data.exists) {
      res.render("pages/404",{username:req.user.username,message: req.flash('message'),title: "Student Details"})
    } else {
      // const student = new Student(
      //   data.id,
      //   data.data().name,
      //   data.data().birthday,
      //   data.data().address,
      //   data.data().phone,
      //   data.data().status,
      //   {field_1: "asd",field_2: "asd"}
      // );
      res.render("pages/getStudent", { student: data.data(), id: data.id ,title:'Student Details',username:req.user.username,message: req.flash('message')});
    }
  } catch (error) {
    res.status(400).send(error.message);
  }
};

const updateStudent = async (req, res) => {
  try {
    const id = req.params.id;
    const data = req.body;
    const reason = data.reason_reject;
    const student = await firestore.collection(collection).doc(id);
    await student.update(data);
    
    if(reason === "không"){
      await firestore.collection(collection).doc('count').update({approve: admin.firestore.FieldValue.increment(1),pending: admin.firestore.FieldValue.increment(-1)})
      req.flash("message","Approved")
    }else{
      await firestore.collection(collection).doc('count').update({reject: admin.firestore.FieldValue.increment(1),pending: admin.firestore.FieldValue.increment(-1)})
      req.flash("message","Rejected")
    }
    res.redirect("/manager");
  } catch (error) {
    res.status(400).send(error.message);
  }
};

const deleteStudent = async (req, res) => {
  try {
    const id = req.params.id;
    const status = req.body.status;
    console.log(status)
    await firestore.collection(collection).doc(id).delete();
    if(status === "0"){
      await firestore.collection(collection).doc('count').update({counters: admin.firestore.FieldValue.increment(-1),pending: admin.firestore.FieldValue.increment(-1)})
    }else if(status === "1" ){
      await firestore.collection(collection).doc('count').update({counters: admin.firestore.FieldValue.increment(-1),approve: admin.firestore.FieldValue.increment(-1)})
    }else if(status==="2"){
      await firestore.collection(collection).doc('count').update({counters: admin.firestore.FieldValue.increment(-1),reject: admin.firestore.FieldValue.increment(-1)})
    }
    req.flash("message","Delete successful")
    res.redirect("/manager");
  } catch (error) {
    res.status(400).send(error.message);
  }
};

function newTime(time){
  const x = time.slice(6) +" " +time.slice(0,5)
  return x
}

module.exports = {
  addStudent,
  getAllStudent,
  getStudent,
  updateStudent,
  deleteStudent,
};
