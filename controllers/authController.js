const passport = require("passport");
const bcrypt = require('bcrypt')

const isLoggedIn = (req, res, next) => {
  if (req.isAuthenticated()) return next();
  res.redirect("/login");
};

const isLoggedOut = (req, res, next) => {
  if (!req.isAuthenticated()) return next();
  res.redirect("/");
};

const passportAuth = passport.authenticate("local", {
  successRedirect: "/",
  failureRedirect: "/login",
  failureFlash: true
});

const logIn = (req, res) => {  
  res.render("pages/login", { message: req.flash('error') ,title:'login'});
};

const logOut = (req, res) => {
  req.logout();
  res.redirect("/");
};

module.exports = {
  isLoggedIn,
  isLoggedOut,
  passportAuth,
  logIn,
  logOut,
};
